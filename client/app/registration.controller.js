(function() {

    "use strict";
    angular.module("regApp").controller("regCtrl", regCtrl);

    regCtrl.$inject = ["$http"];

    function regCtrl($http) {
        var self = this; 

        self.test = "hello there!";

        self.person = {
            email: "",
            password: "",
            confirmPwd: "",
            name: "",
            gender: "",
            dob: "",
            address: "",
            country: "",
            contact: ""

        };

        self.showDetails = false;

        self.testing = "test";

        self.nations = [
            { id: 1, name: 'Singapore' },
            { id: 2, name: 'China' },
            { id: 3, name: 'Japan' },
            { id: 4, name: 'Malaysia' },
            { id: 5, name: 'Indonesia'},
            { id: 6, name: 'Vietnam'}

        ];


        self.submitReg = function(){

          $http.post("/users", self.person) 
                
                .then(function (result) {
                    console.log(result);

                    self.showDetails = result;


                })
                
                .catch(function (e) {
                    console.log(e);
                });

        };




         self.ageCheck = function(){
        
        var incomingDate = new Date(self.person.dob);
        // console.log(incomingDate);
        var getIncomingYear = incomingDate.getFullYear();
        // console.log(getIncomingYear);

        var minDate = new Date(1999, 12, 30);
        var getMinYear = minDate.getFullYear();
        console.log(getMinYear);


        if(getIncomingYear >= getMinYear)
        
        return true;

        // else
        // return false;        


        };
       
       
        // self.toggle = function(){

        //     console.log("toggle");
        //     self.showDetails = !self.showDetails;
        //     console.log(self.showDetails);


        // };


    }

})();
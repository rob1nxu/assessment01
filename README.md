PRE-SETUP
Install Git & Node.js
Install Git

Download installer from https://git-scm.com/download/win
Use default settings
Set up BitBucket account

Go to https://bitbucket.org/account/signup
Select personal repository (instead of team) when prompted
If sharing, uncheck "This is a private repository"
Install Node.js

Download installer from https://nodejs.org/en/download
Get latest Long Term Support (LTS) Version, NOT latest current version
Use default settings
Start local Project directory
Create working directory for project
[Project directory] Create subfolders & files.
E.g.

C:\..\Project dir>md server
C:\..\Project dir>md client
C:\..\Project dir\client>md js
C:\..\Project dir\client>md css
C:\..\Project dir\client>md assets
TIP: To create an empty file from DOS command prompt copy NUL > app.js
Prepare local Project directory for Git
[Project directory] Initialise git git init
[BitBucket.org] Create new remote repository (remote_repo) online
[Project directory] Connect local directory to remote repo git remote add origin git clone remote_repo
E.g.

C:\..\Project dir>git remote add origin git clone https://cheen888@bitbucket.org/cheen888/day03.git
[Project directory] Continue working on folders and files

Sync files to remote_repo

git add * or git add <filename>

git commit -m "Updates for today"

Syntax: git push <remote name> <branch name>
In this case "master" is the main version of the code being worked on locally
git push origin master

SERVER SETUP
Prepare Express.JS environment
[Project directory] Initialise npm npm init
[During npm init or in package.json] When configuring parameters, set entry point to point to server application file (e.g. server/app.js), OR edit package.json to amend "main" property

Entry point: server/app.js

OR

"main": "server/app.js"
[Project directory] Install express npm install express --save

TIP: When using someone else's cloned files, execute npm init to set up all dependencies referenced in package.json
Install global server utilities
Install Nodemon npm install nodemon -g
Install Body Parser npm install body-parser --save
Exclude express and bower files from git sync
[.gitignore] Edit .gitignore file to add "node_modules" & other files to exclude from git sync

<.gitignore file>

node_modules  
client/css/personal_notes.txt
Start server application
[Project directory] fix a specific port (if required) set NODE_PORT=3000
[Project directory] launch node monitor nodemon
[browser] point to server app & specify designated port

URL: http://localhost:3000
Other useful git actions
Make a copy of (clone) someone else's files

Syntax: git clone <repository link>
git clone https://cheen888@bitbucket.org/cheen888/myprojectfiles.git

NOTE: Don't need to create new folder beforehand. Cloning process creates the main folder and sub-folder structure
Check git current fetch and push settings

git remote -v

Branch out from main working code to work on a new feature/issue

3.1 In project directory, create a branch to work on feature18

Syntax: git checkout -b <branch name> - to create a new branch
Syntax: git checkout <branch name> - to switch to an existing branch

git checkout -b feature18

3.2 After completion of edits, submit changes

git add * or git add <filename>

git commit -m "Updates for today"

3.3 Push feature18 up for review and possible merge to main code

Syntax: git push <remote name> <branch name>
In this case "feature18" is the branch of the code being worked on locally

git push origin feature18

Switch back to master version from branch when done with side feature or fix
4.1 Switch back to master branch

git checkout master

4.2 If finished with branch (e.g. feature18), delete it for good housekeeping

git branch -d feature18

If branches have been forgotten, check by running branch call. "*" points to current active branch

git branch

> git branch
    master
    * feature18
    testing
Download the latest master version on the remote repo, when everyone's files have been merged

git pull origin master

NOTE: git pull origin master is the same as git fetch and git merge combined
Other useful express/nodemon actions
Check version of nodemon nodemon --version
Restart nodemon in case of error. From within nodemon prompt, type rs
To kill a running express server

in Windows Task Manager, end ALL node.exe processes
from DOS prompt, taskkill /im node.exe OR taskkill /f /im node.exe ("/f" force)
CLIENT SETUP
Install global client utilities
Install Bower npm install bower -g
Exclude bower files from git sync
[.gitignore] Edit .gitignore file to add"bower_components" & other files to exclude from git sync

<.gitignore file>

node_modules  
bower_components  
client/css/personal_notes.txt
Set up Bower
[Project directory] Create .bowerrc copy NUL > .bowerrc
[In .bowerrc] Edit file to specify location of bower_components

{  
   "directory": "client/bower_components"  
}
[Project directory] Initialise bower.json bower init

Install Angular, Bootstrap & Font Awesome
Install the front-end packages

bower install angular --save

bower install bootstrap --save

bower install font-awesome --save

Note: "--save" appends package dependencies into bower.json
TIP: When using someone else's cloned files, execute bower init to set up all dependencies referenced in bower.json
Install other Angular tools
[web browser] Install Angular inspector for browser extension

Search using keywords "ng inspect for Angular"
E.g. ng-inspector for AngularJS by ng-inspector.org

Install Postman
Install Postman, for visual API development

Download installer from https://www.getpostman.com
Use default settings
Sign up for a free account